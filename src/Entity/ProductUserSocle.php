<?php

namespace ProvisionningMyEC3\Entity;

class ProductUserSocle
{
    private $productId;

    private $userSocleId;

    private $userProductId;

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function setProductId(string $productId): void
    {
        $this->productId = $productId;
    }

    public function getUserSocleId(): string
    {
        return $this->userSocleId;
    }

    public function setUserSocleId(string $userSocleId): void
    {
        $this->userSocleId = $userSocleId;
    }

    public function getUserProductId(): string
    {
        return $this->userProductId;
    }

    public function setUserProductId(string $userProductId): void
    {
        $this->userProductId = $userProductId;
    }
}
