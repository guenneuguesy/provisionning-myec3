<?php

namespace ProvisionningMyEC3\Entity;

class ProductDepartmentSocle
{
    private $productId;

    private $departmentSocleId;

    private $departmentProductId;

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function setProductId(string $productId): self
    {
        $this->productId = $productId;
        return $this;
    }

    public function getDepartmentSocleId(): string
    {
        return $this->departmentSocleId;
    }

    public function setDepartmentSocleId(string $departmentSocleId): self
    {
        $this->departmentSocleId = $departmentSocleId;
        return $this;
    }

    public function getDepartmentProductId(): string
    {
        return $this->departmentProductId;
    }

    public function setDepartmentProductId(string $departmentProductId): self
    {
        $this->departmentProductId = $departmentProductId;
        return $this;
    }
}
