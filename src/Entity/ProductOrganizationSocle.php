<?php

namespace ProvisionningMyEC3\Entity;

class ProductOrganizationSocle
{
    private $productId;

    private $organizationSocleId;

    private $organizationProductId;

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function setProductId(string $productId): void
    {
        $this->productId = $productId;
    }

    public function getOrganizationSocleId(): string
    {
        return $this->organizationSocleId;
    }

    public function setOrganizationSocleId(string $organizationSocleId): void
    {
        $this->organizationSocleId = $organizationSocleId;
    }

    public function getOrganizationProductId(): string
    {
        return $this->organizationProductId;
    }

    public function setOrganizationProductId(string $organizationProductId): void
    {
        $this->organizationProductId = $organizationProductId;
    }
}
