<?php

namespace ProvisionningMyEC3\Product\Pastell;

use Libriciel\Myec3\DepartmentSocleInterface;
use Pastell\Api\EntitesRequester;
use Pastell\Hydrator\EntiteHydrator;
use ProvisionningMyEC3\Entity\ProductDepartmentSocle;
use ProvisionningMyEC3\Exception\ProductOrganizationSocleNotFoundException;
use ProvisionningMyEC3\Repository\ProductDepartmentSocleRepository;
use ProvisionningMyEC3\Repository\ProductOrganizationSocleRepository;
use Psr\Http\Client\ClientExceptionInterface;
use SimpleXMLElement;

class DepartmentProvisioning implements DepartmentSocleInterface
{
    public const PRODUCT_NAME = 'pastell';
    /**
     * @var ProductDepartmentSocleRepository
     */
    private $productDepartmentSocleRepository;
    /**
     * @var ProductOrganizationSocleRepository
     */
    private $productOrganizationSocleRepository;
    /**
     * @var EntitesRequester
     */
    private $entitesRequester;
    /**
     * @var EntiteHydrator
     */
    private $entiteHydrator;


    public function __construct(
        ProductDepartmentSocleRepository $productDepartmentSocleRepository,
        ProductOrganizationSocleRepository $productOrganizationSocleRepository,
        EntitesRequester $entitesRequester,
        EntiteHydrator $entiteHydrator = null
    ) {
        $this->productDepartmentSocleRepository = $productDepartmentSocleRepository;
        $this->productOrganizationSocleRepository = $productOrganizationSocleRepository;
        $this->entitesRequester = $entitesRequester;
        $this->entiteHydrator = $entiteHydrator ?? new EntiteHydrator();
    }

    public function getId(string $externalId)
    {
        if ($this->productDepartmentSocleRepository->exists(self::PRODUCT_NAME, $externalId)) {
            $productOrganizationSocle = $this->productDepartmentSocleRepository->get(self::PRODUCT_NAME, $externalId);
            return $productOrganizationSocle->getDepartmentProductId();
        }
        return null;
    }

    /**
     * @throws ProductOrganizationSocleNotFoundException
     * @throws ClientExceptionInterface
     */
    public function add(SimpleXMLElement $xml): bool
    {
        $productDepartmentSocle = new ProductDepartmentSocle();
        $productDepartmentSocle->setProductId(self::PRODUCT_NAME);
        $productDepartmentSocle->setDepartmentSocleId((string)$xml->externalId);

        if ((string)($xml->rootDepartment) === 'false') {
            $entite = $this->getEntiteFromXml($xml);
            $response = $this->entitesRequester->create($this->entiteHydrator->hydrate($entite));
            $productDepartmentSocle->setDepartmentProductId($response->getId());
        } else {
            // Case when the department is the organism, the pastell entity already exist
            $productDepartmentSocle->setDepartmentProductId(
                $this->productOrganizationSocleRepository->get(
                    self::PRODUCT_NAME,
                    (string)$xml->organism->externalId
                )->getOrganizationProductId()
            );
        }

        return $this->productDepartmentSocleRepository->add($productDepartmentSocle);
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function update(SimpleXMLElement $xml, $departmentId): bool
    {
        $entite = $this->getEntiteFromXml($xml);
        $pastellEntite = $this->entitesRequester->show($departmentId);
        $pastellEntite->denomination = $entite['denomination'];
        $pastellEntite->siren = $entite['siren'];
        $this->entitesRequester->update($pastellEntite);

        return true;
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function delete($departmentId): bool
    {
        $productDepartmentSocle = $this->productDepartmentSocleRepository->getByDepartmentProductId(
            self::PRODUCT_NAME,
            $departmentId
        );

        $this->entitesRequester->remove($departmentId);
        return $this->productDepartmentSocleRepository->delete($productDepartmentSocle);
    }

    private function getEntiteFromXml(SimpleXMLElement $xml): array
    {
        return [
            'socleId' => (string)$xml->externalId,
            'denomination' => (string)$xml->label,
            'siren' => (string)$xml->siren,
            'type' => 'collectivite',
            'entite_mere' => $this->productDepartmentSocleRepository
                ->get(self::PRODUCT_NAME, (string)$xml->parentDepartment->externalId)
                ->getDepartmentProductId(),
        ];
    }
}
