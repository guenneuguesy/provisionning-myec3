<?php

namespace ProvisionningMyEC3\Product\Pastell;

use Exception;
use Libriciel\Myec3\UserSocleInterface;
use Pastell\Api\UserRolesRequester;
use Pastell\Api\UsersRequester;
use Pastell\Exception\PastellException;
use Pastell\Hydrator\UserHydrator;
use Pastell\Model\UserRole;
use ProvisionningMyEC3\Entity\ProductUserSocle;
use ProvisionningMyEC3\Exception\ProductUserSocleNotFoundException;
use ProvisionningMyEC3\Repository\ProductUserSocleRepository;
use Psr\Http\Client\ClientExceptionInterface;
use SimpleXMLElement;

class UserProvisioning implements UserSocleInterface
{
    public const PRODUCT_NAME = 'pastell';

    /**
     * @var ProductUserSocleRepository
     */
    private $productUserSocleRepository;

    /**
     * @var UsersRequester
     */
    private $usersRequester;

    /**
     * @var UserHydrator
     */
    private $userHydrator;

    /**
     * @var UserRolesRequester
     */
    private $userRolesRequester;

    public function __construct(
        ProductUserSocleRepository $productUserSocleRepository,
        UserRolesRequester $userRolesRequester,
        UsersRequester $usersRequester,
        UserHydrator $userHydrator = null
    ) {
        $this->productUserSocleRepository = $productUserSocleRepository;
        $this->userRolesRequester = $userRolesRequester;
        $this->usersRequester = $usersRequester;
        $this->userHydrator = $userHydrator ?? new UserHydrator();
    }

    /**
     * add user
     * @param SimpleXMLElement $xml
     * @param string|int $structureId
     * @param string|int $roleId
     * @return bool
     * @throws ClientExceptionInterface
     * @throws PastellException
     * @throws Exception
     */
    public function add(SimpleXMLElement $xml, $structureId, $roleId): bool
    {
        $user = $this->getUserFromXml($xml);
        $user['password'] = base64_encode(random_bytes(20));
        $user['id_e'] = $structureId;

        $userId = null;
        try {
            $response = $this->usersRequester->create($this->userHydrator->hydrate($user));
            $userId = $response->getId();
        } catch (PastellException $exception) {
            $message = json_decode($exception->getMessage(), true);
            if (!empty($message['error-message']) && $message['error-message'] === 'Ce login existe déjà') {
                $users = $this->usersRequester->all(['id_e' => $structureId]);
                foreach ($users as $currentUser) {
                    if ($currentUser->login === $user['login']) {
                        $userId = $currentUser->getId();
                    }
                }
            }
            if (is_null($userId)) {
                throw $exception;
            }
        }

        $userRole = new UserRole();
        $userRole->role = $roleId;
        $userRole->id_u = $userId;
        $userRole->id_e = $structureId;
        $this->userRolesRequester->add($userRole);

        $productUserSocle = new ProductUserSocle();
        $productUserSocle->setProductId(self::PRODUCT_NAME);
        $productUserSocle->setUserSocleId($user['socleId']);
        $productUserSocle->setUserProductId($userId);
        return $this->productUserSocleRepository->add($productUserSocle);
    }

    /**
     * update user
     * @param SimpleXMLElement $xml
     * @param string|int $userId
     * @param string|int $roleId
     * @return bool
     * @throws ClientExceptionInterface
     */
    public function update(SimpleXMLElement $xml, $userId, $roleId): bool
    {
        $user = $this->getUserFromXml($xml);
        $pastellUser = $this->usersRequester->show($userId);
        $pastellUser->prenom = $user['prenom'];
        $pastellUser->nom = $user['nom'];
        $pastellUser->email = $user['email'];
        $this->usersRequester->update($pastellUser);

        return true;
    }

    /**
     * delete user
     * @param string|int $userId user
     * @return bool
     * @throws ClientExceptionInterface
     */
    public function delete($userId): bool
    {
        $productUserSocle = $this->productUserSocleRepository->getByUserProductId(self::PRODUCT_NAME, $userId);
        $pastellUser = $this->usersRequester->show($userId);
        $pastellUser->login = 'DELETED_' . $pastellUser->login;
        $pastellUser->email = 'DELETED_' . $pastellUser->email;
        $this->usersRequester->update($pastellUser);

        return $this->productUserSocleRepository->delete($productUserSocle);
    }

    /**
     * get user by externalId
     * @param string $externalId
     * @return string|int
     * @throws ProductUserSocleNotFoundException
     */
    public function getId(string $externalId)
    {
        if (!$this->productUserSocleRepository->exists(self::PRODUCT_NAME, $externalId)) {
            return null;
        }
        return $this->productUserSocleRepository->get(self::PRODUCT_NAME, $externalId)->getUserProductId();
    }

    private function getUserFromXml(SimpleXMLElement $xml): array
    {
        return [
            'socleId' => (string)$xml->externalId,
            'login' => (string)$xml->user->username,
            'prenom' => (string)$xml->user->firstname,
            'nom' => (string)$xml->user->lastname,
            'email' => (string)$xml->email,
        ];
    }
}
