<?php

namespace ProvisionningMyEC3\Product\S2low;

use Libriciel\Myec3\UserSocleInterface;
use ProvisionningMyEC3\Entity\ProductUserSocle;
use ProvisionningMyEC3\Exception\ProductUserSocleNotFoundException;
use ProvisionningMyEC3\Http\Client\S2lowClient;
use ProvisionningMyEC3\Http\Client\S2lowClientException;
use ProvisionningMyEC3\Http\Model\S2lowUser;
use ProvisionningMyEC3\Repository\ProductUserSocleRepository;
use Psr\Http\Client\ClientExceptionInterface;
use SimpleXMLElement;

class UserProvisioning implements UserSocleInterface
{
    public const PRODUCT_NAME = 's2low';

    /**
     * @var ProductUserSocleRepository
     */
    private $productUserSocleRepository;
    /**
     * @var S2lowClient
     */
    private $s2lowClient;
    /**
     * @var array
     */
    private $defaultUserValues;

    public function __construct(
        ProductUserSocleRepository $productUserSocleRepository,
        S2lowClient $s2lowClient,
        array $defaultUserValues
    ) {
        $this->productUserSocleRepository = $productUserSocleRepository;
        $this->s2lowClient = $s2lowClient;
        $this->defaultUserValues = $defaultUserValues;
    }

    /**
     * @inheritDoc
     * @throws S2lowClientException
     * @throws ClientExceptionInterface
     * @throws \Exception
     */
    public function add(SimpleXMLElement $xml, $structureId, $roleId): bool
    {
        $user = $this->getUserFromXml($xml);
        $user->authority_id = (int)$structureId;
        $user->authority_group_id = $this->defaultUserValues['authority_group_id'];
        $user->role = $this->defaultUserValues['role'];
        $user->setActPermission('RW');
        $user->setHeliosPermission('RW');

        $response = $this->s2lowClient->createOrUpdateUser($user);
        $userId = $response['id'];
        $services = $this->s2lowClient->getAuthorityServices($structureId);
        if (!empty($services)) {
            $this->s2lowClient->addUserToService($userId, $services[0]['id']);
        }

        $productUserSocle = new ProductUserSocle();
        $productUserSocle->setProductId(self::PRODUCT_NAME);
        $productUserSocle->setUserSocleId((string)$xml->externalId);
        $productUserSocle->setUserProductId($userId);
        return $this->productUserSocleRepository->add($productUserSocle);
    }

    /**
     * @inheritDoc
     * @throws S2lowClientException
     * @throws ClientExceptionInterface
     */
    public function update(SimpleXMLElement $xml, $userId, $roleId): bool
    {
        $user = $this->s2lowClient->getUser($userId);
        $user->name = (string)$xml->user->lastname;
        $user->givenname = (string)$xml->user->firstname;
        $user->email = (string)$xml->email;
        $user->certificate = (string)$xml->user->certificate;
        $this->s2lowClient->createOrUpdateUser($user);

        // $roleId contains the id of the organism so we don't have to heavily modify the Socle class
        $services = $this->s2lowClient->getAuthorityServices($roleId);
        if (!empty($services)) {
            $this->s2lowClient->addUserToService($userId, $services[0]['id']);
        }

        return true;
    }

    /**
     * @inheritDoc
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function delete($userId): bool
    {
        $productUserSocle = $this->productUserSocleRepository->getByUserProductId(self::PRODUCT_NAME, $userId);
        $user = $this->s2lowClient->getUser($userId);
        $user->status = 0;
        $this->s2lowClient->createOrUpdateUser($user);

        return $this->productUserSocleRepository->delete($productUserSocle);
    }

    /**
     * @inheritDoc
     * @throws ProductUserSocleNotFoundException
     */
    public function getId(string $externalId)
    {
        if (!$this->productUserSocleRepository->exists(self::PRODUCT_NAME, $externalId)) {
            return null;
        }
        return $this->productUserSocleRepository->get(self::PRODUCT_NAME, $externalId)->getUserProductId();
    }

    /**
     * @throws \Exception
     */
    private function getUserFromXml(SimpleXMLElement $xml): S2lowUser
    {
        $user = new S2lowUser();
        $user->name = (string)$xml->user->lastname;
        $user->givenname = (string)$xml->user->firstname;
        $user->email = (string)$xml->email;
        $user->certificate = (string)$xml->user->certificate;
        $user->status = 1;
        $user->auth_method = 1;

        return $user;
    }
}
