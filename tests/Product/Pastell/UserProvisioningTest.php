<?php

namespace ProvisionningMyEC3\Tests\Product\Pastell;

use Pastell\Api\UserRolesRequester;
use Pastell\Api\UsersRequester;
use Pastell\Exception\PastellException;
use Pastell\Hydrator\UserHydrator;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ProvisionningMyEC3\Entity\ProductUserSocle;
use ProvisionningMyEC3\Exception\ProductUserSocleNotFoundException;
use ProvisionningMyEC3\Product\Pastell\UserProvisioning;
use ProvisionningMyEC3\Repository\ProductUserSocleRepository;
use Psr\Http\Client\ClientExceptionInterface;

class UserProvisioningTest extends TestCase
{

    /**
     * @var UserHydrator
     */
    private $userHydrator;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->userHydrator = new UserHydrator();
    }

    private function getProductUserSocle(
        string $product,
        string $socleId,
        string $productId
    ): ProductUserSocle {
        $productOrganizationSocle = new ProductUserSocle();
        $productOrganizationSocle->setProductId($product);
        $productOrganizationSocle->setUserSocleId($socleId);
        $productOrganizationSocle->setUserProductId($productId);
        return $productOrganizationSocle;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws PastellException
     */
    public function testAdd()
    {
        $productUserSocle = $this->getProductUserSocle(UserProvisioning::PRODUCT_NAME, '300005473', 123);

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this->getMockBuilder(ProductUserSocleRepository::class)->getMock();
        $productUserSocleRepository->expects($this->once())
            ->method('add')
            ->with($productUserSocle)
            ->willReturn(true);

        /** @var MockObject|UserRolesRequester $userRolesRequester */
        $userRolesRequester = $this->getMockBuilder(UserRolesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|UsersRequester $usersRequesterMock */
        $usersRequesterMock = $this->getMockBuilder(UsersRequester::class)
            ->disableOriginalConstructor()
            ->getMock();
        $usersRequesterMock->expects($this->once())
            ->method('create')
            ->willReturnCallback(function ($arg) {
                $this->assertEquals(12, $arg->id_e);
                return $this->userHydrator->hydrate([
                    'id_u' => 123,
                    'login' => 'klauss_heissler',
                    'prenom' => 'Klaus',
                    'nom' => 'Heissler',
                    'email' => 'klausheissler@gmail.com',
                ]);
            });

        $provisioningUser = new UserProvisioning($productUserSocleRepository, $userRolesRequester, $usersRequesterMock);
        $this->assertTrue(
            $provisioningUser->add(
                simplexml_load_file(__DIR__ . '/../../fixtures/agent.xml'),
                12,
                ""
            )
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws PastellException
     */
    public function testAddDuplicatedPastellLogin()
    {
        $productUserSocle = $this->getProductUserSocle(UserProvisioning::PRODUCT_NAME, '300005473', 123);

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this->getMockBuilder(ProductUserSocleRepository::class)->getMock();
        $productUserSocleRepository->expects($this->once())
            ->method('add')
            ->with($productUserSocle)
            ->willReturn(true);

        /** @var MockObject|UserRolesRequester $userRolesRequester */
        $userRolesRequester = $this->getMockBuilder(UserRolesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|UsersRequester $usersRequesterMock */
        $usersRequesterMock = $this->getMockBuilder(UsersRequester::class)
            ->disableOriginalConstructor()
            ->getMock();
        $usersRequesterMock->expects($this->once())
            ->method('create')
            ->willThrowException(new PastellException(json_encode(['error-message' => 'Ce login existe déjà'])));
        $usersRequesterMock->expects($this->once())
            ->method('all')
            ->willReturn([
                $this->userHydrator->hydrate([
                    'login' => 'klauss_heissler',
                    'id_u' => 123,
                    'email' => 'mail'
                ])
            ]);


        $provisioningUser = new UserProvisioning($productUserSocleRepository, $userRolesRequester, $usersRequesterMock);
        $this->assertTrue(
            $provisioningUser->add(
                simplexml_load_file(__DIR__ . '/../../fixtures/agent.xml'),
                12,
                ""
            )
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testUpdate()
    {
        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this->getMockBuilder(ProductUserSocleRepository::class)->getMock();

        /** @var MockObject|UserRolesRequester $userRolesRequester */
        $userRolesRequester = $this->getMockBuilder(UserRolesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|UsersRequester $usersRequesterMock */
        $usersRequesterMock = $this->getMockBuilder(UsersRequester::class)
            ->disableOriginalConstructor()
            ->getMock();
        $usersRequesterMock->expects($this->once())
            ->method('show')
            ->with(123)
            ->willReturn($this->userHydrator->hydrate([
                'id_u' => 123,
                'login' => 'klauss_heissler',
                'prenom' => 'Klaus',
                'nom' => 'Heissler',
                'email' => 'klausheissler@gmail.com',
            ]));

        $provisioningUser = new UserProvisioning($productUserSocleRepository, $userRolesRequester, $usersRequesterMock);
        $this->assertTrue(
            $provisioningUser->update(simplexml_load_file(__DIR__ . '/../../fixtures/agent.xml'), 123, "")
        );
    }


    /**
     * @throws ClientExceptionInterface
     */
    public function testDelete()
    {
        $productUserSocle = $this->getProductUserSocle(UserProvisioning::PRODUCT_NAME, '300005473', 123);
        $user = $this->userHydrator->hydrate([
            'id_u' => 123,
            'login' => 'klauss_heissler',
            'prenom' => 'Klaus',
            'nom' => 'Heissler',
            'email' => 'klausheissler@gmail.com',
        ]);

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this->getMockBuilder(ProductUserSocleRepository::class)->getMock();
        $productUserSocleRepository->expects($this->once())
            ->method('getByUserProductId')
            ->with(UserProvisioning::PRODUCT_NAME, 123)
            ->willReturn($productUserSocle);
        $productUserSocleRepository->expects($this->once())
            ->method('delete')
            ->willReturn(true);

        /** @var MockObject|UserRolesRequester $userRolesRequester */
        $userRolesRequester = $this->getMockBuilder(UserRolesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|UsersRequester $usersRequesterMock */
        $usersRequesterMock = $this->getMockBuilder(UsersRequester::class)
            ->disableOriginalConstructor()
            ->getMock();
        $usersRequesterMock->expects($this->once())
            ->method('show')
            ->with(123)
            ->willReturn($user);

        $user->login = 'DELETED_' . $user->login;
        $user->email = 'DELETED_' . $user->email;

        $usersRequesterMock->expects($this->once())
            ->method('update')
            ->with($user)
            ->willReturn($user);

        $provisioningUser = new UserProvisioning($productUserSocleRepository, $userRolesRequester, $usersRequesterMock);
        $this->assertTrue($provisioningUser->delete(123));
    }

    /**
     * @throws ProductUserSocleNotFoundException
     */
    public function testGetId()
    {
        $productUserSocle = $this->getProductUserSocle(UserProvisioning::PRODUCT_NAME, 7, 123);

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this->getMockBuilder(ProductUserSocleRepository::class)->getMock();
        $productUserSocleRepository->expects($this->once())
            ->method('exists')
            ->willReturn(true);
        $productUserSocleRepository->expects($this->once())
            ->method('get')
            ->with(UserProvisioning::PRODUCT_NAME, 7)
            ->willReturn($productUserSocle);

        /** @var MockObject|UserRolesRequester $userRolesRequester */
        $userRolesRequester = $this->getMockBuilder(UserRolesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|UsersRequester $usersRequester */
        $usersRequester = $this->getMockBuilder(UsersRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        $provisioningUser = new UserProvisioning($productUserSocleRepository, $userRolesRequester, $usersRequester);
        $this->assertEquals(123, $provisioningUser->getId(7));
    }

    /**
     * @throws ProductUserSocleNotFoundException
     */
    public function testGetNotExistingUserId()
    {

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this->getMockBuilder(ProductUserSocleRepository::class)->getMock();
        $productUserSocleRepository->expects($this->once())
            ->method('exists')
            ->willReturn(false);

        /** @var MockObject|UserRolesRequester $userRolesRequester */
        $userRolesRequester = $this->getMockBuilder(UserRolesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|UsersRequester $usersRequester */
        $usersRequester = $this->getMockBuilder(UsersRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        $provisioningUser = new UserProvisioning($productUserSocleRepository, $userRolesRequester, $usersRequester);
        $this->assertNull($provisioningUser->getId(7));
    }
}
