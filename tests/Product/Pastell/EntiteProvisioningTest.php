<?php

namespace ProvisionningMyEC3\Tests\Product;

use Pastell\Api\EntitesRequester;
use Pastell\Hydrator\EntiteHydrator;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ProvisionningMyEC3\Entity\ProductOrganizationSocle;
use ProvisionningMyEC3\Exception\ProductOrganizationSocleNotFoundException;
use ProvisionningMyEC3\Product\Pastell\EntiteProvisioning;
use ProvisionningMyEC3\Repository\ProductOrganizationSocleRepository;
use Psr\Http\Client\ClientExceptionInterface;

class EntiteProvisioningTest extends TestCase
{

    /**
     * @var EntiteHydrator
     */
    private $entiteHydrator;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->entiteHydrator = new EntiteHydrator();
    }

    /**
     * @param string $product
     * @param string $socleId
     * @param string $productId
     * @return ProductOrganizationSocle
     */
    private function getProductOrganizationSocle(
        string $product,
        string $socleId,
        string $productId
    ): ProductOrganizationSocle {
        $productOrganizationSocle = new ProductOrganizationSocle();
        $productOrganizationSocle->setProductId($product);
        $productOrganizationSocle->setOrganizationSocleId($socleId);
        $productOrganizationSocle->setOrganizationProductId($productId);
        return $productOrganizationSocle;
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testAdd()
    {
        $productOrganizationSocle = $this->getProductOrganizationSocle(
            EntiteProvisioning::PRODUCT_NAME,
            '100000005',
            123
        );

        /** @var MockObject|ProductOrganizationSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this->getMockBuilder(ProductOrganizationSocleRepository::class)->getMock();
        $productUserSocleRepository->expects($this->once())
            ->method('add')
            ->with($productOrganizationSocle)
            ->willReturn(true);

        /** @var MockObject|EntitesRequester $entitesRequesterMock */
        $entitesRequesterMock = $this->getMockBuilder(EntitesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();
        $entitesRequesterMock->expects($this->once())
            ->method('create')
            ->willReturn($this->entiteHydrator->hydrate([
                'id_e' => 123,
                'denomination' => 'name',
                'siren' => '000000000',
                'type' => 'collectivite',
                'entite_mere' => '0'
            ]));

        $provisioningUser = new EntiteProvisioning($productUserSocleRepository, $entitesRequesterMock);
        $this->assertTrue(
            $provisioningUser->add(simplexml_load_file(__DIR__ . '/../../fixtures/organism.xml'))
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testUpdate()
    {
        /** @var MockObject|ProductOrganizationSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this->getMockBuilder(ProductOrganizationSocleRepository::class)->getMock();

        /** @var MockObject|EntitesRequester $entitesRequesterMock */
        $entitesRequesterMock = $this->getMockBuilder(EntitesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();
        $entitesRequesterMock->expects($this->once())
            ->method('show')
            ->with(123)
            ->willReturn($this->entiteHydrator->hydrate([
                'id_e' => 123,
                'denomination' => 'name',
                'siren' => '000000000',
                'type' => 'collectivite',
                'entite_mere' => '0'
            ]));

        $provisioningUser = new EntiteProvisioning($productUserSocleRepository, $entitesRequesterMock);
        $this->assertTrue(
            $provisioningUser->update(simplexml_load_file(__DIR__ . '/../../fixtures/organism.xml'), 123)
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testDelete()
    {
        $productOrganizationSocle = $this->getProductOrganizationSocle(EntiteProvisioning::PRODUCT_NAME, 5, 123);

        /** @var MockObject|ProductOrganizationSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this->getMockBuilder(ProductOrganizationSocleRepository::class)->getMock();
        $productUserSocleRepository->expects($this->once())
            ->method('getByOrganizationProductId')
            ->with(EntiteProvisioning::PRODUCT_NAME, 123)
            ->willReturn($productOrganizationSocle);
        $productUserSocleRepository->expects($this->once())
            ->method('delete')
            ->willReturn(true);

        /** @var MockObject|EntitesRequester $entitesRequesterMock */
        $entitesRequesterMock = $this->getMockBuilder(EntitesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        $provisioningUser = new EntiteProvisioning($productUserSocleRepository, $entitesRequesterMock);
        $this->assertTrue($provisioningUser->delete(123));
    }


    /**
     * @throws ProductOrganizationSocleNotFoundException
     */
    public function testGetId()
    {
        $productOrganizationSocle = $this->getProductOrganizationSocle(EntiteProvisioning::PRODUCT_NAME, 5, 123);

        /** @var MockObject|ProductOrganizationSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this->getMockBuilder(ProductOrganizationSocleRepository::class)->getMock();
        $productUserSocleRepository->expects($this->once())
            ->method('exists')
            ->willReturn(true);
        $productUserSocleRepository->expects($this->once())
            ->method('get')
            ->with(EntiteProvisioning::PRODUCT_NAME, 5)
            ->willReturn($productOrganizationSocle);

        /** @var MockObject|EntitesRequester $entitesRequesterMock */
        $entitesRequesterMock = $this->getMockBuilder(EntitesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        $provisioningUser = new EntiteProvisioning($productUserSocleRepository, $entitesRequesterMock);
        $this->assertEquals(123, $provisioningUser->getId(5));
    }

    /**
     * @throws ProductOrganizationSocleNotFoundException
     */
    public function testGetNotExistingOrganismId()
    {
        /** @var MockObject|ProductOrganizationSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this->getMockBuilder(ProductOrganizationSocleRepository::class)->getMock();
        $productUserSocleRepository->expects($this->once())
            ->method('exists')
            ->willReturn(false);

        /** @var MockObject|EntitesRequester $entitesRequesterMock */
        $entitesRequesterMock = $this->getMockBuilder(EntitesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        $provisioningUser = new EntiteProvisioning($productUserSocleRepository, $entitesRequesterMock);
        $this->assertNull($provisioningUser->getId(5));
    }
}
